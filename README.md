A boring video of the turret being controlled via Axis in Linuxcnc: https://youtu.be/2dME0hAw2PU

This component is based on the work of Tome (tom-emc@bgp.nu), which has been generalized to be also applicable for:
- turret with any number of tools (as long smaller then 15);
- stepper motor instead of a DC motor;
- optional homing of the turret on the first tool change.

The toolerator-component can be used as well with the original toolerator3000-skecth (DC-motor based, fixed with 8 tools, designed for the EMCO 120P) and the toolerator500-sketch (stepper motor based, configurable number of tools, deigned for the EMCO 5 CNC).

Put the component (toolerator.comp) in your Linuxcnc configuration file directory. 
In a terminal type: 
```bash
sudo halcompile --install toolerator.comp
```

This will compile and install the component for your use. You probably need to have the linuxcnc-dev package installed.

Add to your *.hal file (could be custom.hal, or your main hal file):
```
loadusr -W toolerator

net tool-change iocontrol.0.tool-change => toolerator.0.toolchange
net tool-changed iocontrol.0.tool-changed <= toolerator.0.toolchanged
net tool-number iocontrol.0.tool-prep-number => toolerator.0.toolnumber
```