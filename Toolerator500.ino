/*
Toolerator 5000
Tool changer for Emco 5 CNC
Early style turret, 6 tool position, with the original DC Motor replaced by a stepper motor. Can be extended for other turrets with
a maximum of 15 tools, thus can also be used for a EMCO 120 lathe with 8 tools. The firmware can run in two modes:
- assuming the tool at start-up is toolnumber 1;
- requireing a home switch to detect the position of tool number 1. When the first tool change is requested by the controller, the 
  tool post will first home itself to find the index of tool number 1. To enable this, the pin for HOME_SWITCH should be defined.

Uses a stepper motor driver, Polulu A4988, Polulu DRV8825, or Leadshine DM-series.

Requires the StepperDriver library
pins:
D0:  (tx)
D1:  (rx)
D2: Stepper drive pulse/step pin
D3: Stepper drive direction pin
D4: Stepper drive enable pin (optional)

*/
#include <BasicStepperDriver.h>

#define VERSION_STRING "Toolerator 500 version 1.0 Drunken Dog"

// Definitions for the communication and toolposy
#define BAUDRATE 9600     // The baudrate for communication with LinuxCNC. Should be equal to the baudrate defined in toolerator500.comp
#define TOOL_COUNT 6      // The maximum number of tools in the toolerator. For the EMCO 5 CNC this is 6 tools, for the EMCO 120 this number is 8 tools
#define GEAR_REDUCTION 30 // The reduction of the (worm-)gear in the toolpost.
#define OVER_TRAVEL 10    // The amount of rotation required by the toolpost in order for the ratchet to lock (in degrees).

// Definitions for the stepper motor
#define MOTOR_STEPS 200   // Number of full steps of the stepper for one full revolution, should be rarely changed. 
#define MOTOR_RPM 100     // The maximum RPM of the motor.
#define MOTOR_MICROSTEP 8 // The micro-stepping setting of the motor
#define MOTOR_ACC 2000    // Acceleration of the tool changer (steps/s²)
#define MOTOR_DEC 2000    // Deceleration of the tool changer (steps/s²)
#define MOTOR_PIN_STP 2   // Pin for sending steps signal on
#define MOTOR_PIN_DIR 3   // Pin for sending direction (CW/CCW) signal on
//#define MOTOR_PIN_ENA 4   // Pin for enabling/disabling the stepper motor
//#define ENA_ACTIVE_STATE HIGH   // Defines whether the enable pin should be active high or active low 

// Definitions for the home switch. The home sequence will only will be executed when the HOME_SWITCH_PIN is defined.
//TODO: home switch is not yet correctly implemented, update 
//#define HOME_SWITCH_PIN 5 // Pin where the home switch is located
//#define HOME_SWITCH_INTERNAL_PULLUP // Uncomment this setting to enable the internal pullup for the homing switch
//#define HOME_SWITCH_ACTIVE LOW // Defines when the home switch is triggered, either HIGH or LOW

// Calculated definitions
// - the number of steps required to change the tool one position
#define TOOL_CHANGE_STEPS long(MOTOR_STEPS) * MOTOR_MICROSTEP * GEAR_REDUCTION / TOOL_COUNT
// - then number of steps required for the defined over travel (distance which is covered back and forward.
#define OVER_TRAVEL_STEPS long(MOTOR_STEPS) * MOTOR_MICROSTEP * GEAR_REDUCTION * OVER_TRAVEL / 360


// Enumerations of the state of the Toolerator
enum states { 
  initState, 
  listeningState,
  homingState,
  moveForwardToPositionState, 
  moveReverseToLockState,
  queryState, 
  versionState
} state;

// Create a instance of the stepper driver
#ifdef MOTOR_PIN_ENA
  BasicStepperDriver stepper(MOTOR_STEPS, MOTOR_PIN_DIR, MOTOR_PIN_STP, MOTOR_PIN_ENA);
#else
  BasicStepperDriver stepper(MOTOR_STEPS, MOTOR_PIN_DIR, MOTOR_PIN_STP);
#endif
// The current tool which is loaded. It is assumed that the tool number on start-up equals to tool 1
// because there is no position feedback (encoder) on the EMCO 5 CNC
uint8_t currentTool=1;
uint8_t targetTool=1;
uint8_t compensation=0;


// Setup the toolerator 500
void setup() {
  // Create a velocity profile for the stepper (trapezium shaped)
  stepper.setSpeedProfile(BasicStepperDriver::LINEAR_SPEED, MOTOR_ACC, MOTOR_DEC);
#if defined(MOTOR_PIN_ENA) && defined(ENA_ACTIVE_STATE)
  stepper.setEnableActiveState(ENA_ACTIVE_STATE);
#endif
  stepper.begin(MOTOR_RPM, MOTOR_MICROSTEP);

#ifdef HOME_SWITCH_PIN
  // When defined, create the input for the homing switch
  pinMode(HOME_SWITCH_PIN, INPUT); 
#endif
#ifdef HOME_SWITCH_INTERNAL_PULLUP
  // When defined, activate the internal pullup on the pin by writing to the port
  digitalWrite(HOME_SWITCH_PIN, HIGH); 
#endif

  // Listen on the serial port for tool change commands
  Serial.begin(BAUDRATE);

  // Set the state to initial state;
  state=initState;
}

void loop() {
  switch(state) {
    case initState:
      handleInitState();
      break;
    case listeningState:
      handleListeningState();
      break;
#ifdef HOME_SWITCH_PIN
    case homingState:
      if (!stepper.nextAction()) {
        // Homing failed, disable stepper and send out error message
        stepper.disable();
        Serial.write('X');
      } else {
        if (digitalRead(HOME_SWITCH_PIN) == HOME_SWITCH_ACTIVE) {
          currentTool=1;
          startMoveToTool();
        }
      }
      break;
#endif
    case moveForwardToPositionState:
      // Make steps until no steps are available. In that case reverse to lock the cam
      if (!stepper.nextAction()) {
        stepper.startMove(-1 * OVER_TRAVEL_STEPS);
        state=moveReverseToLockState;
      }
      break;
    case moveReverseToLockState:
      // Make steps in reverse direction until no steps are avaailable. In that case the
      // cam is locked in place and the success is reported to the controller.
      if (!stepper.nextAction()) {
        currentTool = targetTool;
        Serial.write('0'+currentTool);
        state=listeningState;
      }
      break;
    case queryState:
      Serial.write( '0'+ currentTool );
      state=listeningState;
      break;
    case versionState:
      Serial.println(VERSION_STRING);
      state=listeningState;
      break;
  }
}


/*
 * This is the state which is enetered after initialisation of the toolerator. It
 * will write a ready message o the Serial and put the state in listening model
 */
void handleInitState() {
  // print "T" for Toolerator 500--ready message
  Serial.write('T');
  stepper.enable();
  state=listeningState;
}


/*
 *  This function listens for commands over the USB. When a tool-change is 
 *  commanded, this will command the stepper motor to take action
 */
void handleListeningState() {
  if (Serial.available() > 0) {
    byte b = Serial.read(); // Read 1 byte from the buffer
    switch(b) {
      case 'Q':
        state=queryState;
        break;
      case 'V':
        state=versionState;
        break;
#if (TOOL_COUNT <= 1)
      case '1':
#endif
#if (TOOL_COUNT <= 2)
      case '2':
#endif
#if (TOOL_COUNT <= 3)
      case '3':
#endif
#if (TOOL_COUNT <= 4)
      case '4':
#endif
#if (TOOL_COUNT <= 5)
      case '5':
#endif
#if (TOOL_COUNT <= 6)
      case '6':
#endif
#if (TOOL_COUNT <= 7)
      case '7':
#endif
#if (TOOL_COUNT <= 8)
      case '8':
#endif
#if (TOOL_COUNT <= 9)
      case '9':
#endif
#if (TOOL_COUNT <= 10)
      case 'A':
#endif
#if (TOOL_COUNT <= 11)
      case 'B':
#endif
#if (TOOL_COUNT <= 12)
      case 'C':
#endif
#if (TOOL_COUNT <= 13)
      case 'D':
#endif
#if (TOOL_COUNT <= 14)
      case 'E':
#endif
#if (TOOL_COUNT <= 15)
      case 'F':
#endif
        targetTool=b-'0';
        if( currentTool==targetTool) {
          // No need to move to a new position
          state=listeningState;
          Serial.write('0'+currentTool);
#ifdef HOME_SWITCH_PIN
        } else if (currentTool == 0) { 
          // The tool should be homed first by rotating a full circle
          stepper.startMove(TOOL_COUNT * TOOL_CHANGE_STEPS);
          state=homingState;
#endif
        } else {
          startMoveToTool();
        }
        break;
      default:
        // Unknown command, report back and don't change the state
        Serial.write('?');
        break;
    }
  }
}

/*
 * Function which calculates how many segments the toolerator has to rotate, while
 * taking into account any rounding errors which may occur during the proceess. 
 */
void startMoveToTool() {
  // Determine the number of positions the toolerator has to rotate
  int32_t positions = targetTool - currentTool;
  if (positions < 0) {
    positions += TOOL_COUNT;   
  }
  stepper.startMove(positions * TOOL_CHANGE_STEPS + OVER_TRAVEL_STEPS);
  state=moveForwardToPositionState;
}
